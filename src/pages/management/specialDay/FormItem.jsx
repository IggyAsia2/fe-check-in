import React from 'react';
import { ProFormText, ProFormDateRangePicker } from '@ant-design/pro-form';

const FormItem = () => {
  return (
    <>
      <ProFormDateRangePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        width="md"
        label=" Tên ngày nghỉ "
        name="name"
      />
      <ProFormText width="md" label=" Ghi chú " name="note" />
    </>
  );
};

export default FormItem;
