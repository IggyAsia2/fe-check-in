import React, { useRef } from 'react';
import Child from '@/components/Table/specialDay/child';
import ChildTable from '@/components/Table/specialDay/childTable';
import FormItem from './FormItem';
import FormItem2 from './FormItem2';
import moment from 'moment';
import { getAllSDay, createSDay, updateSDay, removeSDay, createHoliday } from '@/api/specialDayApi';
import { useRequest } from 'umi';

const TableList = () => {
  const name = 'Special Day';
  const { run } = useRequest(createHoliday, {
    manual: true,
  });

  const childColumn = [
    {
      title: 'Ngày',
      dataIndex: 'date',
      render: (date) => moment(date).format('DD/MM/YYYY'),
    },
    {
      title: 'Tên',
      dataIndex: 'name',
    },
    {
      title: 'Ghi chú',
      dataIndex: 'note',
    },
  ];

  return (
    <ChildTable
      FormItem={FormItem}
      FormItem2={FormItem2}
      name={name}
      childColumn={childColumn}
      getAllData={getAllSDay}
      getHoliday={(e) => run(e)}
      createData={createSDay}
      updateData={updateSDay}
      removeData={removeSDay}
      Child={Child}
      extendButton={true}
    />
  );
};

export default TableList;
