import React from 'react';
import { ProFormText, ProFormDatePicker } from '@ant-design/pro-form';

const FormItem2 = () => {
  return (
    <>
      <ProFormDatePicker
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        format="DD/MM/YYYY"
        width="md"
        label=" Chọn ngày "
        name="date"
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        width="md"
        label=" Tên ngày nghỉ "
        name="name"
      />
      <ProFormText width="md" label=" Ghi chú " name="note" />
    </>
  );
};

export default FormItem2;
