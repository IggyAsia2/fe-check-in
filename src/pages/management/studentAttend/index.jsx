import React, { useEffect, useState } from 'react';
import Child from '@/components/Table/studentAttend/child';
import ChildTable from '@/components/Table/studentAttend/childTable';
import FormItem from './FormItem';
import FormItem2 from './FormItem2';
import moment from 'moment';
import {
  getAllStudentAttend,
  getAllStudents,
  createStudentAttend,
  updateStudentAttend,
  removeStudentAttend,
  getStatusOption,
  exportFile,
} from '@/api/studentAttendApi';
import { useRequest } from 'umi';

const sDay = {
  X: 'Đi làm',
  T7: 'Thứ 7',
  CN: 'Chủ nhật',
  NL: 'Ngày lễ',
};

const TableList = () => {
  const name = 'Student Attendance';

  const [studentData, setStudentData] = useState(null);
  const { data: optionData, run: optionRun } = useRequest(getStatusOption, {
    manual: true,
    formatResult: (res) =>
      res.data.map((el) => {
        return { label: el.name, value: el.val };
      }),
  });
  const { data: studentAttendData, run: studentAttendRun } = useRequest(getAllStudentAttend, {
    manual: true,
    formatResult: (res) => {
      return {
        ...res,
        data: res.data.map((el) => {
          const check = optionData.find((i) => i.value === el.status);
          return {
            ...el,
            status: {
              label: check ? check.label : sDay[el.status],
              value: el.status,
            },
            Student: { label: el.Student.name, value: el.Student.id, code: el.Student.code },
          };
        }),
      };
    },
  });

  useEffect(() => {
    optionRun(1);
    fetchStudentData();
  }, []);

  // useEffect(() => {
  //   console.log(studentAttendData);
  // }, [studentAttendData]);

  const fetchStudentData = async () => {
    const res = await getAllStudents();
    setStudentData(
      res.data.map((el) => {
        return {
          label: el.name,
          value: el.id,
        };
      }),
    );
  };

  const childColumn = [
    {
      title: 'Họ và tên',
      dataIndex: ['Student', 'label'],
      // render: (date) => moment(date).format('DD/MM/YYYY'),
    },
    {
      title: 'Status',
      dataIndex: ['status', 'label'],
    },
    {
      title: 'Ghi chú',
      dataIndex: 'note',
    },
    {
      title: 'Người tạo',
      dataIndex: 'createdby',
    },
    {
      title: 'Date',
      dataIndex: 'date',
      render: (date) => moment(date).format('DD/MM/YYYY'),
    },
  ];

  if (optionData && optionData.length) {
    return (
      <ChildTable
        FormItem={FormItem}
        FormItem2={FormItem2}
        name={name}
        childColumn={childColumn}
        childData={studentData}
        childData2={optionData}
        getAllData={studentAttendRun}
        createData={createStudentAttend}
        updateData={updateStudentAttend}
        removeData={removeStudentAttend}
        Child={Child}
        extendButton={true}
        exportFile={exportFile}
      />
    );
  }
  return '';
};

export default TableList;
