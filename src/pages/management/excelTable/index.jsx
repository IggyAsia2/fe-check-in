import React, { useState } from 'react';
import Child from '@/components/Table/excelTable/child';
import ChildTable from '@/components/Table/excelTable/childTable';
import { saveAllAttendance } from '@/api/studentAttendApi';

const SubChild = ({ fromDay, toDay, columns, numDay, migraData, setMigraData, statusData }) => {
  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  const [currentRow, setCurrentRow] = useState(undefined);

  const name = 'Student Attendance';

  return (
    <ChildTable
      name={name}
      childColumn={columns}
      numDay={numDay}
      migraData={migraData}
      setMigraData={setMigraData}
      saveData={saveAllAttendance}
      Child={Child}
      fromDay={fromDay}
      toDay={toDay}
      updateModalVisible={updateModalVisible}
      handleUpdateModalVisible={handleUpdateModalVisible}
      currentRow={currentRow}
      setCurrentRow={setCurrentRow}
      statusData={statusData}
    />
  );
};

export default SubChild;
