import React from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { message } from 'antd';

const CreateForm = ({
  FormItem,
  childData,
  childData2,
  actionRef,
  name,
  createModalVisible,
  handleModalVisible,
  createData,
}) => {
  const handleAdd = async (fields) => {
    const hide = message.loading('Loading...');

    try {
      await createData({ ...fields });
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  return (
    <ModalForm
      title={`Create ${name}`}
      width="400px"
      visible={createModalVisible}
      onVisibleChange={handleModalVisible}
      onFinish={async (value) => {
        const success = await handleAdd(value);
        if (success) {
          handleModalVisible(false);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <FormItem childData={childData} childData2={childData2} />
    </ModalForm>
  );
};

export default CreateForm;
