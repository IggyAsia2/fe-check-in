import React from 'react';
import { ModalForm } from '@ant-design/pro-form';
import { message } from 'antd';

const UpdateForm = ({
  childData,
  childData2,
  FormItem,
  actionRef,
  name,
  visible,
  setVisible,
  current,
  onDone,
  updateData,
}) => {
  const handleUpdate = async (fields) => {
    const hide = message.loading('Loading...');

    try {
      await updateData({ ...fields }, current.id);
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  if (!visible) {
    return null;
  }

  return (
    <ModalForm
      title={`Update ${name}`}
      initialValues={current}
      width="400px"
      visible={visible}
      modalProps={{
        onCancel: () => onDone(),
        destroyOnClose: true,
      }}
      onFinish={async (value) => {
        const success = await handleUpdate(value);
        if (success) {
          setVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <FormItem childData={childData} childData2={childData2} />
    </ModalForm>
  );
};

export default UpdateForm;
