import React from 'react';
import { ModalForm, ProFormSelect } from '@ant-design/pro-form';
import { Card } from 'antd';
import moment from 'moment';

const { Meta } = Card;

const UpdateForm = ({
  childData,
  visible,
  setVisible,
  current,
  onDone,
  setMigraData,
  migraData,
  updateData,
  viewModalVisible,
  setLoading,
}) => {
  let thisDay;
  if (current && current.item) {
    thisDay = new Date(current.item.eventdate);
  }

  if (!visible) {
    return null;
  }

  const fetchUpdate = async (status, id) => {
    const res = await updateData({ status: status }, id);
  };

  return (
    <ModalForm
      title={`${moment(thisDay).format('DD/MM/YYYY')} ${
        current.item.eventtime !== null ? current.item.eventtime : ''
      }`}
      width="400px"
      visible={visible}
      modalProps={{
        onCancel: () => onDone(),
        destroyOnClose: true,
      }}
      initialValues={{
        status: {
          label: childData.find((i) => i.value === current.item.val).label,
          value: current.item.val,
        },
      }}
      onFinish={(value) => {
        setVisible(false);

        if (!viewModalVisible) {
          fetchUpdate(
            typeof value.status === 'object' ? value.status.value : value.status,
            current.item.attendance_id,
          );
        }

        let newArr = [...migraData];
        newArr[current.index] = {
          ...newArr[current.index],
          [current.day]: {
            ...newArr[current.index][current.day],
            val: typeof value.status === 'object' ? value.status.value : value.status,
            isChange: true,
          },
        };
        setMigraData(newArr);
      }}
    >
      <Meta title={`${current.studentname} | ${current.orgunit}`} description={current.item.note} />
      <br />

      <ProFormSelect
        rules={[
          {
            required: true,
            message: 'Require',
          },
        ]}
        options={childData}
        width="md"
        label=" Status "
        name="status"
      />
      <Card cover={<img src={current.item.imgurl} />} />
    </ModalForm>
  );
};

export default UpdateForm;
