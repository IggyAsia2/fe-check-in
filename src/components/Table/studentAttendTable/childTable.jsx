import React, { useState, useRef } from 'react';
import { message, Popconfirm } from 'antd';
import { useRequest } from 'umi';
import { getStatusOption } from '@/api/studentAttendApi';
import CreateForm from './components/CreateForm';
import UpdateForm from './components/UpdateForm';
import ViewForm from './components/ViewForm';
import { useEffect } from 'react';

const ChildTable = ({
  optionData,
  childData,
  FormItem,
  name,
  childColumn,
  getAllData,
  removeData,
  createData,
  updateData,
  Child,
  fromDay,
  toDay,
  setFromDay,
  setToDay,
  getCol,
  updateModalVisible,
  handleUpdateModalVisible,
  currentRow,
  setCurrentRow,
  saveData,
  numDay,
  migraData,
  setMigraData,
  loading,
  setLoading,
}) => {
  const actionRef = useRef();

  const { data: statusData, run: optionRun } = useRequest(getStatusOption, {
    manual: true,
    formatResult: (res) =>
      res.data.map((el) => {
        return { label: el.name, value: el.val };
      }),
  });

  useEffect(() => {
    optionRun(1);
  }, []);

  const [createModalVisible, handleModalVisible] = useState(false);
  const [viewModalVisible, handleViewModalVisible] = useState(false);
  const [onOk, setOnOk] = useState(false);

  const handleRemove = async (selectedRows) => {
    const hide = message.loading('Loading...');
    if (!selectedRows) return true;

    try {
      for (let i of selectedRows) {
        await removeData(i.id);
      }
      hide();
      message.success('Success');
      return true;
    } catch (error) {
      hide();
      message.error('Fail, Please try again!');
      return false;
    }
  };

  const createModal = (
    <CreateForm
      FormItem={FormItem}
      childData={childData}
      name={name}
      createModalVisible={createModalVisible}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      createData={createData}
      fromDay={fromDay}
      toDay={toDay}
      setFromDay={setFromDay}
      setToDay={setToDay}
      getCol={getCol}
      getAllData={getAllData}
      setOnOk={setOnOk}
    />
  );

  const handleDone = () => {
    handleUpdateModalVisible(false);
    setCurrentRow({});
  };

  const updateModal = (
    <UpdateForm
      FormItem={FormItem}
      childData={statusData}
      setMigraData={setMigraData}
      migraData={migraData}
      name={name}
      visible={updateModalVisible}
      setVisible={handleUpdateModalVisible}
      current={currentRow}
      onDone={handleDone}
      actionRef={actionRef}
      updateData={updateData}
      viewModalVisible={viewModalVisible}
      setLoading={setLoading}
    />
  );

  const viewModal = (
    <ViewForm
      visible={viewModalVisible}
      setVisible={handleViewModalVisible}
      fromDay={fromDay}
      toDay={toDay}
      columns={childColumn}
      numDay={numDay}
      optionData={optionData}
      migraData={migraData}
      setMigraData={setMigraData}
      onOk={onOk}
      setOnOk={setOnOk}
      statusData={statusData}
      saveData={saveData}
      getAllData={getAllData}
      loading={loading}
      setLoading={setLoading}
    />
  );

  return (
    <Child
      name={name}
      columns={childColumn}
      updateModalVisible={updateModalVisible}
      handleUpdateModalVisible={handleUpdateModalVisible}
      createModalVisible={createModalVisible}
      handleModalVisible={handleModalVisible}
      actionRef={actionRef}
      handleRemove={handleRemove}
      createModal={createModal}
      updateModal={updateModal}
      viewModal={viewModal}
      fromDay={fromDay}
      toDay={toDay}
      optionData={optionData}
      saveData={saveData}
      numDay={numDay}
      migraData={migraData}
      loading={loading}
      getAllData={getAllData}
      visible={viewModalVisible}
      setVisible={handleViewModalVisible}
      onOk={onOk}
      setOnOk={setOnOk}
    />
  );
};

export default ChildTable;
