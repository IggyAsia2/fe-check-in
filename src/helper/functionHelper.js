import Cookies from 'js-cookie';
import jwt from 'jwt-decode';

const getUserId = () => {
  const token = Cookies.get('usertoken');
  if (token) {
    const user = jwt(token);
    if (user) return user.id;
    return -1;
  }
};

const getUserToken = (bool) => {
  if (bool)
    return {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${Cookies.get('usertoken')}`,
    };
  return {
    'Content-Type': 'application/json',
  };
};

export { getUserId, getUserToken };
